

/*****************************
          Anounces
 ****************************/
    var $ = jQuery;
    jQuery(document).ready(function($){
      $('.anounce-wrap').slick({
        dots: false,
        infinite: true,
        autoplaySpeed: 10000,
        slidesToShow: 4,
        slidesToScroll: 2,
        autoplay: true,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2, 
              infinite: true,
              dots: true
            }
          },
          
        ]
      });
    });

    /*****************************
          Entreprises
 ****************************/
          var $ = jQuery;
          jQuery(document).ready(function($){
            $('.entreprise-wrap').slick({
              dots: false,
              infinite: true,
              autoplaySpeed: 10000,
              slidesToShow: 4,
              slidesToScroll: 2,
              autoplay: true,
              responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 2, 
                    infinite: true,
                    dots: true
                  }
                },
                
              ]
            });
          });